'use strict';

angular.module('myApp.admin', ['ngRoute', 'firebase', 'ngMask', 'oc.modal', 'datePicker'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/admin', {
            templateUrl: 'admin/admin.html',
            controller: 'AdminCtrl'
        });
    }])

    .controller('AdminCtrl',
        ['$scope', 'fireService', 'Utils', '$ocModal', '$timeout',
            function ($scope, fireService, Utils, $ocModal, $timeout) {
                // scope
                $scope.titulo = 'JSDay FSA Admin';
                $scope.trilhas = [];
                $scope.palestras = [];
                $scope.workshops = [];
                $scope.parseDate = _parseDate;
                $scope.addTrilhaClick = _addTrilhaClick;
                $scope.addPalestraClick = _addPalestraClick;
                $scope.addWorkshopClick = _addWorkshopClick;
                $scope.palestraClick = _palestraClick;
                $scope.workshopClick = _workshopClick;
                $scope.trilhaChange = _trilhaChange;

                _getTrilhas();
                _getWorkshops()
                //fireService.mockPalestras();

                $scope.$on('palestraDeleted', function(e){
                    _getPalestrasByTrilha($scope.trilha.$id);
                });
                $scope.$on('palestraAdded', function(e){
                    _getPalestrasByTrilha($scope.trilha.$id);
                });
                $scope.$on('workshopAdded', function(e){
                    _getPalestrasByTrilha($scope.trilha.$id);
                });

                ///////////////////////////////////

                function _getTrilhas() {
                    $scope.trilhas = fireService.getTrilhas();
                    $scope.trilhas.$loaded(function(resolve){
                        if ($scope.trilhas.length) {
                            $scope.trilha = $scope.trilhas[0];
                            $timeout(function(){
                                _getPalestrasByTrilha($scope.trilha.$id);
                            },300);
                        }
                    });
                }
                function _getPalestrasByTrilha(_key) {
                    $scope.palestras = fireService.getPalestrasByTrilha(_key);
                }

                function _getWorkshops(){
                    $scope.workshops = fireService.getWorks();
                    $scope.workshops.$loaded(function(resolve){
                        //
                    });
                }

                function _trilhaChange() {
                    _getPalestrasByTrilha($scope.trilha.$id);
                }

                function _parseDate(_data) {
                    if (typeof _data == 'number')
                        return moment(_data).format('DD/MM/YYYY HH:mm');

                    return _data;
                }

                function _addPalestraClick() {
                    $ocModal.open({
                        url: 'admin/cad-palestra.html',
                        cls: 'slide-down modal-cad-palestra',
                        controller: 'cadPalestraCtrl',
                        init: {
                            trilhas: $scope.trilhas
                        }
                    });
                }

                function _addTrilhaClick() {
                    $ocModal.open({
                        url: 'admin/cad-trilha.html',
                        cls: 'slide-down modal-cad-trilha',
                        controller: 'cadTrilhaCtrl'
                    });
                }

                function _addWorkshopClick() {
                    $ocModal.open({
                        url: 'admin/cad-workshop.html',
                        cls: 'slide-down modal-cad-workshop',
                        controller: 'cadWorkshopCtrl',
                        init: {
                            workshop: null
                        }
                    });
                    //fireService.mockWorks();
                }

                function _workshopClick(_work){
                    $ocModal.open({
                        url: 'admin/cad-workshop.html',
                        cls: 'slide-down modal-cad-workshop',
                        controller: 'cadWorkshopCtrl',
                        init: {
                            workshop: _work
                        }
                    });
                }

                function _palestraClick(_palestra) {
                    var redes = {};

                    if (!_palestra.servico && _palestra.autor.redes)
                        _palestra.autor.redes.forEach(function (_v) {
                            switch (_v.tipo) {
                                case 'email':
                                    redes.email = _v.valor;
                                    break;
                                case 'link':
                                    redes.link = _v.valor;
                                    break;
                                case 'social-github':
                                    redes.github = _v.valor;
                                    break;
                                case 'social-twitter':
                                    redes.twitter = _v.valor;
                                    break;
                            }
                        });
                    /*_palestra.hora = Utils.parseDateToStr(_palestra.hora, true);

                    $scope.novo = _palestra;
                    $scope.redes = redes;*/

                    $ocModal.open({
                        url: 'admin/cad-palestra.html',
                        cls: 'slide-down modal-cad-palestra',
                        controller: 'cadPalestraCtrl',
                        init: {
                            palestra: _palestra,
                            redes: redes,
                            trilhas: $scope.trilhas
                        }
                    });
                }
            }
        ]
    )

    .controller('cadTrilhaCtrl',
        ['$scope', 'fireService', '$ocModal',
            function ($scope, fireService, $ocModal) {
                $scope.data = {
                    dia: moment()
                };

                $scope.salvarTrilha = function () {
                    $scope.trilha.timestamp = $scope.data.dia.valueOf();
                    fireService.addTrilha($scope.trilha);
                    $ocModal.close();
                };
            }
        ]
    )

    .controller('cadPalestraCtrl',
        ['$scope', '$rootScope', 'fireService', '$ocModal', '$init',
            function ($scope, $rootScope, fireService, $ocModal, $init) {
                $scope.novo = $init.palestra || {};
                $scope.redes = $init.redes || {};
                $scope.data = {
                    dia: $scope.novo.hora ? moment($scope.novo.hora) : moment()
                };
                $scope.trilhas = $init.trilhas;
                $scope.trilha = $scope.trilhas.length ? $scope.trilhas[0] : {};
                $scope.salvarPalestra = _salvarPalestra;
                $scope.excluirClick = _excluirClick;

                _trilhaAtual();

                /////////////////////////////

                $scope.$watch('trilha', function () {
                    //console.log('trilha selected');
                    $scope.data.dia = $scope.novo.hora ? moment($scope.novo.hora) : $scope.trilha ? moment($scope.trilha.timestamp) : moment();
                });

                function _salvarPalestra() {
                    var _id = $scope.novo.$id || 0;

                    if (!$scope.novo.servico) {
                        $scope.novo.autor.redes = [];
                        $scope.novo.avaliacao = $scope.novo.avaliacao || {total: 0, votos: 0};
                    }
                    $scope.novo.hora = $scope.data.dia.valueOf();

                    if (!$scope.novo.servico)
                        Object.keys($scope.redes).forEach(function (_key) {
                            if (_key == 'email') $scope.novo.autor.redes.push({ tipo: 'email', valor: $scope.redes.email });
                            if (_key == 'link') $scope.novo.autor.redes.push({ tipo: 'link', valor: $scope.redes.link });
                            if (_key == 'github') $scope.novo.autor.redes.push({ tipo: 'social-github', valor: $scope.redes.github });
                            if (_key == 'twitter') $scope.novo.autor.redes.push({ tipo: 'social-twitter', valor: $scope.redes.twitter });
                        });

                    if (typeof $scope.novo.hora == 'string'){
                        $scope.novo.hora = moment($scope.novo.hora, 'DD/MM/YYYY HH:mm').valueOf();
                    }

                    if (_id === 0)
                        try {
                            fireService.addPalestra($scope.trilha.$id, $scope.novo)
                                .then(function(onSuccess){
                                    $ocModal.close();
                                    $rootScope.$broadcast('palestraAdded', {success: true});
                                }, function (onReject) {
                                    console.log(onReject);
                                });

                        } catch (e) {
                            alert('erro ao salvar a palestra.\nConsulte o console para mais detalhes.');
                            console.log(e);
                        }
                    else
                        try {
                            fireService.setPalestra($scope.novo);
                            $ocModal.close();
                        } catch (e) {
                            alert('erro ao salvar a palestra.\nConsulte o console para mais detalhes.');
                            console.log(e);
                        }
                }

                function _excluirClick(_palestra) {
                    fireService.delPalestra(_palestra);
                    $ocModal.close();
                }

                function _trilhaAtual() {
                    if ($scope.novo.$id){
                        var tmp = $scope.trilhas.filter(function (_t) {
                            //console.log(_t.$id, $scope.novo.trilha);
                            return _t.$id == $scope.novo.trilha;
                        });//console.log(tmp);
                        if (tmp.length)
                            $scope.trilha = tmp[0];
                    }
                }
            }
        ]
    )

    .controller('cadWorkshopCtrl',
        ['$scope', '$rootScope', 'fireService', '$ocModal', '$init',
            function ($scope, $rootScope, fireService, $ocModal, $init) {
                $scope.novo = $init.workshop || {};
                $scope.autores = {};
                $scope.data = {
                    horaInicio: $scope.novo.horaInicio ? moment($scope.novo.horaInicio) : moment(),
                    horaFim: $scope.novo.horaFim ? moment($scope.novo.horaFim) : moment()
                };
                $scope.salvarWorkshop = _salvarWorkshop;
                $scope.excluirClick = _excluirClick;

                _init();

                /////////////////////////////

                function _salvarWorkshop() {
                    var _id = $scope.novo.$id || 0;

                    $scope.novo.horaInicio = $scope.data.horaInicio.valueOf();
                    $scope.novo.horaFim = $scope.data.horaFim.valueOf();

                    if (typeof $scope.novo.horaInicio == 'string'){
                        $scope.novo.horaInicio = moment($scope.novo.horaInicio, 'DD/MM/YYYY HH:mm').valueOf();
                    }
                    if (typeof $scope.novo.horaFim == 'string'){
                        $scope.novo.horaFim = moment($scope.novo.horaFim, 'DD/MM/YYYY HH:mm').valueOf();
                    }

                    $scope.novo.autor = [];
                    $scope.novo.autor.push({
                        nome: $scope.autores.autor1,
                        avatar: $scope.autores.avatar1
                    });

                    if ($scope.autores.autor2 && $scope.autores.avatar2)
                        $scope.novo.autor.push({
                            nome: $scope.autores.autor2,
                            avatar: $scope.autores.avatar2
                        });

                    if (_id === 0)
                        try {
                            fireService.addWorks($scope.novo)
                                .then(function(onSuccess){
                                    $ocModal.close();
                                    $rootScope.$broadcast('workshopAdded', {success: true});
                                }, function (onReject) {
                                    console.log(onReject);
                                });

                        } catch (e) {
                            alert('erro ao salvar o workshop.\nConsulte o console para mais detalhes.');
                            console.log(e);
                        }
                    else
                        try {
                            fireService.setWorks($scope.novo);
                            $ocModal.close();
                        } catch (e) {
                            alert('erro ao salvar a palestra.\nConsulte o console para mais detalhes.');
                            console.log(e);
                        }
                }

                function _excluirClick(_work) {
                    fireService.delWorks(_work);
                    $ocModal.close();
                }

                function _init(){
                    if ($scope.novo.$id){
                        $scope.autores.autor1 = $scope.novo.autor[0].nome;
                        $scope.autores.avatar1 = $scope.novo.autor[0].avatar;

                        if ($scope.novo.autor.length > 1){
                            $scope.autores.autor2 = $scope.novo.autor[1].nome;
                            $scope.autores.avatar2 = $scope.novo.autor[1].avatar;
                        }
                    }
                }
            }
        ]
    )
;