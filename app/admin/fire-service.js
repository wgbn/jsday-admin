(function () {
    'use strict';

    /**
     * @description Serviçoa de abstração do Firebase
     * @author Walter Gandarella <walter.wgbn@gmail.com>
     * @memberof jsday
     * @version 1.0.0
     */
    angular
        .module('myApp.admin')
        .factory('fireService', fireService);

    fireService.$inject = ['$firebaseArray', '$rootScope', 'Utils'];

    /**
     * @memberof jsday
     * @ngdoc factory
     * @name fireService
     * @param {provider} $firebaseArray Abstração do Firebase Array
     * @param {factory} Utils Conjunto de funções de utilidade
     * @description
     *   Manipula os dados e faz o acesso á base do Firebase
     */
    function fireService($firebaseArray, $rootScope, Utils) {

        // Initialize Firebase
        var config = {
            apiKey: "AIzaSyBdYNwQollKcyF6hQaJCfsVwKA0_GMi9Yc",
            authDomain: "jsday-app.firebaseapp.com",
            databaseURL: "https://jsday-app.firebaseio.com",
            storageBucket: "jsday-app.appspot.com"
        };
        var db = firebase.initializeApp(config);
        var ref = firebase.database().ref();
        var trilhas = $firebaseArray(ref.child('trilhas'));
        var palestras = $firebaseArray(ref.child('palestras').orderByChild('hora'));
        var works = $firebaseArray(ref.child('workshops'));

        var _return = {
            getTrilhas:     _getTrilhas,
            getTrilhaByKey: _getTrilhaByKey,
            addTrilha:      _addTrilha,
            delTrilha:      _delTrilha,

            getPalestras:       _getPalestras,
            getPalestrasByTrilha:_getPalestrasByTrilha,
            getPalestraByKey:   _getPalestraByKey,
            addPalestra:        _addPalestra,
            setPalestra:        _setPalestra,
            delPalestra:        _delPalestra,

            getComentarios: _getComentarios,
            addComentario:  _addComentario,

            getWorks:       _getWorks,
            addWorks:       _addWorks,
            delWorks:       _delWorks,
            setWorks:       _setWorks,
            mockWorks:      _mockWorks,

            mockPalestras:  _mockPalestra,
            mockWork:       _mockWork
        };

        return _return;

        ///////// TRILHAS /////////

        function _getTrilhas() {
            return trilhas;
        }

        function _getTrilhaByKey(_key) {
            var tmp = trilhas.filter(function (_t) {
                return _t.$id == _key;
            });
            return tmp.length ? tmp[0] : null;
        }

        function _addTrilha(_trilha) {
            return trilhas.$add(_trilha);
        }

        function _delTrilha(_key) {
            return ref.child('trilhas').child(_key).remove();
        }

        ///////// PALESTRAS ///////////

        function _getPalestras() {
            return palestras;
        }

        function _getPalestrasByTrilha(_trilhaKey) {
            return palestras.filter(function (_p) {
                return _p.trilha == _trilhaKey;
            });
        }

        function _getPalestraByKey(_key) {
            var tmp = palestras.filter(function (_p) {
                return _p.$id == _key;
            });
            return tmp.length ? tmp[0] : null;
        }

        function _addPalestra(_trilha, _palestra) {
            _palestra.trilha = _trilha;
            return palestras.$add(_palestra);
        }

        function _setPalestra(_palestra) {
            var _index = palestras.$indexFor(_palestra.$id);
            if (_index > -1){
                palestras[_index] = _palestra;
                palestras.$save(_index).then(function (success) {
                    console.log('success', success.key === palestras[_index].$id);
                    $rootScope.$broadcast('palestraAdded', {success: true});
                })
            }
        }

        function _delPalestra(_palestra) {
            return palestras.$remove(_palestra)
                .then(function (success) {
                    $rootScope.$broadcast('palestraDeleted', {success: true});
                });
        }

        //////////// COMENTARIOS ////////////

        function _addComentario(_palestra, _comentario) {
            var _dt = new Date();

            _comentario.registro = _dt.getTime();
            _palestra.comentarios = _palestra.comentarios || {};
            _palestra.comentarios[_dt.getTime().toString()] = _comentario;

            ref.child('palestras').child(_palestra.$id).child("comentarios").child(_dt.getTime().toString()).set(_comentario);
            return _getComentarios(_palestra);
        }

        function _getComentarios(_palestra) {
            if (!_palestra.comentarios)
                return {};

            var res = [];
            Object.keys(_palestra.comentarios).forEach(function (_key) {
                res.push(_palestra.comentarios[_key]);
            });
            return res;
        }

        //////////// WORKSHOPS /////////////

        function _getWorks() {
            return works;
        }

        function _addWorks(_work) {
            return works.$add(_work);
        }

        function _delWorks(_work) {
            return ref.child('workshops').child(_work.$id).remove();
        }

        function _setWorks(_work) {
            var _index = works.$indexFor(_work.$id);
            if (_index > -1){
                works[_index] = _work;
                works.$save(_index).then(function (success) {
                    console.log('success', success.key === works[_index].$id);
                    $rootScope.$broadcast('workshopAdded', {success: true});
                })
            }
        }

        function _mockWorks() {
            var work = {
                autor: [
                    {
                        nome: "Walter Gandarella",
                        avatar: "url",
                    },
                    {
                        nome: "Theo Santana",
                        avatar: "url"
                    }
                ],
                capa: "url",
                descricao: "desc",
                hora: moment('2016-07-23 10:20:00').valueOf(),
                nome: 'nome'
            };
            _addWorks(work);
        }

        //////////// MOCKUPS ////////////

        function _mockPalestra() {
            var p1 = {
                hora: moment('2016-07-23 08:00').valueOf(),
                nome: 'Abertura',
                servico: true
            };

            var p2 = {
                hora: moment('2016-07-23 09:00').valueOf(),
                nome: 'JsDay App',
                avaliacao: {
                    total: 0,
                    votos: 0
                },
                capa: 'http://',
                descricao: 'lorem ipsum',
                slide: 'http://',
                autor: {
                    avatar: 'http://',
                    bio: 'lorem ipsum',
                    nome: 'Walter Gandarella',
                    redes: [
                        {
                            tipo: 'social-twitter',
                            valor: '@jebinha'
                        },
                        {
                            tipo: 'social-github',
                            valor: 'http://github.com/wgbn'
                        }
                    ]
                }
            };

            var t1 = {
                timestamp: moment('2016-07-23').valueOf(),
                nome: 'Trilha 1'
            };

            var t2 = {
                timestamp: moment('2016-07-23').valueOf(),
                nome: 'Trilha 2'
            };

            trilhas.$add(t1)
                .then(function (onSuccess) {
                    p1.trilha = onSuccess.key;
                    p2.trilha = onSuccess.key;
                    palestras.$add(p1);
                    palestras.$add(p2);
                }, function (onReject) {
                    console.log('onReject', onReject);
                }, function (onProgress) {
                    console.log('onProgress', onProgress);
                });

            trilhas.$add(t2)
                .then(function (onSuccess) {
                    p1.trilha = onSuccess.key;
                    p2.trilha = onSuccess.key;
                    palestras.$add(p1);
                    palestras.$add(p2);
                }, function (onReject) {
                    console.log('onReject', onReject);
                }, function (onProgress) {
                    console.log('onProgress', onProgress);
                });
        }

        function _mockWork() {
            var w = {
                timestamp: moment().valueOf(),
                tema: 'AngularJS',
                autor: {
                    avatar: 'http://',
                    bio: 'lorem ipsum',
                    nome: 'Walter Gandarella',
                    redes: [
                        {
                            tipo: 'social-twitter',
                            valor: '@jebinha'
                        },
                        {
                            tipo: 'social-github',
                            valor: 'http://github.com/wgbn'
                        }
                    ]
                },
                avaliacao: {
                    total: 0,
                    votos: 0
                },
                capa: 'http://',
                descricao: 'lorem ipsum'
            }
        }

        function sortPalestras(a,b) {
            if (a.hora < b.hora)
                return -1;
            else if (a.hora > b.hora)
                return 1;
            else
                return 0;
        }
    }
})();